/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int get_occurence(char* str)
{	
	char* keyword;
	char* query = strtok(str, ";");

	/* walk through other tokens */
	if( query != NULL ) 
	{
		keyword = strtok(NULL, ";");
	}
	keyword[strlen(keyword)-1] = 0;
	char* tmp_query = query;
	int count = 0;
	tmp_query = strstr(tmp_query,keyword);
	while((tmp_query != 0) && (*tmp_query != '\0')) 
	{
		++count;
		++tmp_query;
		tmp_query = strstr(tmp_query, keyword);
	}
	return count;
	//return (get_tokenizedoccurence(query, keyword));
}

int main(int argc, char *argv[])
{
	int sockfd, newsockfd, portno;
	socklen_t clilen;
	char buffer[256];
	struct sockaddr_in serv_addr, cli_addr;
	int n;
	int num_occurences;
	if (argc < 2) {
		fprintf(stderr,"ERROR, no port provided\n");
		exit(1);
	}
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) 
		error("ERROR opening socket");
	bzero((char *) &serv_addr, sizeof(serv_addr));
	portno = atoi(argv[1]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);
	if (bind(sockfd, (struct sockaddr *) &serv_addr,
				sizeof(serv_addr)) < 0) 
		error("ERROR on binding");
	listen(sockfd,5);
	clilen = sizeof(cli_addr);
	newsockfd = accept(sockfd, 
			(struct sockaddr *) &cli_addr, 
			&clilen);
	if (newsockfd < 0) 
		error("ERROR on accept");
	bzero(buffer,256);
	n = read(newsockfd,buffer,255);
	if (n < 0) error("ERROR reading from socket");
	num_occurences = get_occurence(buffer);
	sprintf(buffer,"%d",num_occurences);	
	printf("Here is the message: %s\n",buffer);
	n = write(newsockfd,buffer,strlen(buffer));
	if (n < 0) error("ERROR writing to socket");
	close(newsockfd);
	close(sockfd);
	return 0; 
}
